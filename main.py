import itertools
import sys
import typing

from marshmallow.exceptions import ValidationError

from orders import entities, helpers

# fixme: move to somewhere from globals
order_book = dict()
buy_deck = dict()  # OrderedDict may be better
sell_deck = dict()  # OrderedDict may be better


def parse(
        row: typing.Sequence
) -> typing.Union[
    entities.LimitOrder,
    entities.IcebergOrder
]:
    for order_cls in (
            entities.LimitOrder,
            entities.IcebergOrder,
    ):
        try:
            order = helpers.validate_and_create_order(
                row,
                order_cls
            )
        except (TypeError, ValidationError) as exc:
            continue
        else:
            break
    if order is None:
        raise Exception('Unknown order')
    return order


def place_in_deck(
        order: typing.Union[
            entities.LimitOrder,
            entities.IcebergOrder
        ]
) -> None:
    if order.id in order_book:
        raise Exception('Already present in order book')
    if order.type == 'B':
        seq = buy_deck
    elif order.type == 'S':
        seq = sell_deck
    else:
        raise Exception('Invalid order type')
    seq[order.id] = order
    order_book[order.id] = order


def match_order(
        order: typing.Union[
            entities.LimitOrder,
            entities.IcebergOrder
        ]
) -> typing.MutableSequence[
    typing.Union[
        entities.LimitOrder,
        entities.IcebergOrder
    ]
]:
    if order.type == 'B':
        match_deck = sell_deck
    elif order.type == 'S':
        match_deck = buy_deck
    else:
        raise Exception('Invalid order type')

    matched = list()
    for i in range(len(match_deck)):
        vol, matched = recursive(
            list(match_deck.values())[i:],  # not the best
            order.price,
            order.price * order.quantity
        )
        if vol == 0:
            break
    return matched


def recursive(orders, price_limit, target_volume, chunks=None):
    chunks = chunks or []
    for i, o in enumerate(orders):
        if target_volume <= 0 or not orders:
            break
        if o.price == price_limit:
            volume = o.price * o.quantity
            if volume <= target_volume:
                chunks.append(o)
                target_volume, chunks = recursive(
                    orders[i+1:],
                    price_limit,
                    target_volume - volume,
                    chunks
                )
    return target_volume, chunks


def write_the_matched(order, matched_orders):
    for m in matched_orders:
        if order.type == 'B':
            buy_deck.pop(order.id, None)
            print(f'{order.id},{m.id},{m.price},{m.price * m.quantity}')
            sell_deck.pop(m.id, None)
        else:
            sell_deck.pop(order.id, None)
            print(f'{m.id},{order.id},{m.price},{m.price * m.quantity}')
            buy_deck.pop(m.id, None)


def write_the_book() -> None:
    buy_orders = sorted(
        (order for order in order_book.values() if order.type == 'B'),
        key=lambda o: (o.price, o.creation_timestamp),
        reverse=True
    )
    sell_orders = sorted(
        (order for order in order_book.values() if order.type == 'S'),
        key=lambda o: (o.price, o.creation_timestamp),
        reverse=True
    )
    print('+' + '-'*65 + '+')
    print('| {:<31}| {:<31}|'.format('BUY', 'SELL'))
    for c in 'Id,Volume,Price,Price,Volume,Id'.split(','):
        print('| {:<9}'.format(c), end='')
    print('|')
    print('+' + '-' * 65 + '+')
    for buy_order, sell_order in itertools.zip_longest(buy_orders, sell_orders):
        print(
            '|{:>10}|{}|{}|{}|{}|{:>10}|'.format(
                '' if buy_order is None else buy_order.id,
                ' ' * 10 if buy_order is None else '{:>10,}'.format(buy_order.visible_volume),
                ' ' * 10 if buy_order is None else '{:>10,}'.format(buy_order.price),
                ' ' * 10 if sell_order is None else '{:>10,}'.format(sell_order.price),
                ' ' * 10 if sell_order is None else '{:>10,}'.format(sell_order.visible_volume),
                '' if sell_order is None else sell_order.id,
            )
        )
    print('+' + '-' * 65 + '+')


def main() -> None:
    command = input("")
    if command.lstrip() == '' or command.lstrip().startswith('#'):
        pass
    else:
        # create order
        order = parse(command.split(','))
        # place the order in deck
        place_in_deck(order)
        # find the matched orders
        match = match_order(order)
        # print the match
        write_the_matched(order, match)
        # output the order book
        write_the_book()


if __name__ == '__main__':
    # noinspection PyBroadException
    while True:
        try:
            main()
        except Exception as exc:
            sys.stderr.write(f'{exc}')
