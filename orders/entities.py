from dataclasses import dataclass

from . import generics
from . import validators


@dataclass
class LimitOrder(
    generics.BaseOrder,
    generics.SchemaValidatorMixin
):

    @property
    def validator_cls(self):
        return validators.LimitOrderSchema

    @property
    def visible_volume(self):
        return self.price * self.quantity


@dataclass
class IcebergOrder(
    generics.BaseOrder,
    generics.SchemaValidatorMixin
):
    peak_size: int

    @property
    def validator_cls(self):
        return validators.IcebergOrderSchema

    @property
    def visible_volume(self):
        return self.price * self.peak_size
