from marshmallow import Schema, fields, validate, ValidationError


def above_zero(value):
    if value <= 0:
        raise ValidationError(
            "Value should be more than 0."
        )


class BaseOrderSchema(Schema):
    type = fields.String(
        validate=validate.OneOf(('B', 'S'))
    )
    id = fields.Integer(
        validate=above_zero
    )
    price = fields.Integer(
        validate=above_zero
    )
    quantity = fields.Integer(
        validate=above_zero
    )


class LimitOrderSchema(BaseOrderSchema):
    ...


class IcebergOrderSchema(BaseOrderSchema):
    peak_size = fields.Integer(
        validate=above_zero
    )
