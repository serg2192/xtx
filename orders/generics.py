import abc
from dataclasses import dataclass, asdict
import datetime
import typing


@dataclass
class BaseOrder:
    type: str
    id: int
    price: int
    quantity: int

    def __post_init__(self):
        self.creation_timestamp = datetime.datetime.now()

    def as_dict(self):
        return asdict(self)


class ValidatorABC:

    @property
    @abc.abstractmethod
    def validator_cls(self):
        raise NotImplementedError


class SchemaValidatorMixin:

    def validate(self, fields: typing.Mapping) -> typing.MutableMapping:
        return self.validator_cls().load(fields)
