import typing

from marshmallow.exceptions import ValidationError

from .entities import LimitOrder, IcebergOrder


def validate_and_create_order(
        raw_data: typing.Sequence,
        order_cls: typing.Callable
) -> typing.Union[
    LimitOrder,
    IcebergOrder
]:
    # fixme: double initialization overhead
    order = order_cls(*raw_data)
    try:
        order_data = order.validate(order.as_dict())
        order = order_cls(**order_data)
    except ValidationError as ess:
        raise
    return order
